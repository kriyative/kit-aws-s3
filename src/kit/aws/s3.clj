(ns kit.aws.s3
  (:require
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [cognitect.anomalies :as ca]
   [kit.util :as u]
   [kit.log :as log]
   [kit.aws.s3.core :refer [init-client! invoke-client]]
   [kit.aws.s3.aws]
   [kit.aws.s3.mock]))

(defonce client (atom nil))

(defn init!
  ([]
   (init! {:client-type :aws}))
  ([conf]
   (reset! client (init-client! conf))))

;; (reset! client nil)
;; (init! {:client-type :aws})

(defn invoke
  ([op]
   (invoke op nil))
  ([op request]
   (let [result (invoke-client @client
                               {:op (csk/->PascalCaseKeyword op)
                                :request (cske/transform-keys
                                          csk/->PascalCaseKeyword
                                          request)})
         {anomaly ::ca/category} result]
     (if anomaly
       (throw (ex-info "AWS operation failed"
                       {:op op
                        :request request
                        :result result
                        :anomaly anomaly}))
       (cske/transform-keys csk/->kebab-case-keyword result)))))

(defn parse-s3-url
  "parse an s3 URL into a bucket and key"
  [s3-url]
  (rest (re-matches #"[sS]3://([^/]+)/(.*)" s3-url)))

(defn s3-url [bucket key]
  (str "s3://" bucket "/" key))

(defn bucket-location [bucket]
  (invoke :get-bucket-location {:bucket bucket}))

(defn bucket-exists? [bucket]
  (try
    (boolean (bucket-location bucket))
    (catch Exception e
      (when-not (= ::ca/not-found (-> e ex-data :anomaly))
        (throw e))
      false)))

(defn create-bucket! [bucket]
  (invoke :create-bucket {:bucket bucket}))

(defn list-buckets []
  (invoke :list-buckets))

(defn delete-bucket! [bucket]
  (invoke :delete-bucket {:bucket bucket}))

(defn put-object!
  ([url data]
   (let [[bucket key] (parse-s3-url url)]
     (put-object! bucket key data)))
  ([bucket key data]
   (invoke :put-object
           {:bucket bucket
            :key (name key)
            :body data})))

(def read-buffer-size 1024)

(defn get-object
  ([url]
   (apply get-object (parse-s3-url url)))
  ([bucket key]
   (let [object (invoke :get-object {:bucket bucket :key (name key)})]
     object)))

(defn get-object-bytes
  ([url]
   (apply get-object-bytes (parse-s3-url url)))
  ([bucket key]
   (let [object (get-object bucket key)
         {bis :body} object
         result (loop [acc []]
                  (let [buffer (byte-array read-buffer-size)
                        bytes-read (.read bis buffer)]
                    (if-not (= -1 bytes-read)
                      (recur (concat acc (take bytes-read buffer)))
                      acc)))]
     (assoc object :body (into-array Byte/TYPE (map byte result))))))

(defn get-object-str
  ([url]
   (apply get-object-str (parse-s3-url url)))
  ([bucket key]
   (-> (get-object-bytes bucket key)
       (update :body #(String. %)))))

(defn delete-object!
  ([url]
   (apply delete-object! (parse-s3-url url)))
  ([bucket key]
   (invoke :delete-object {:bucket bucket :key (name key)})))

(defn list-objects
  ([url]
   (apply list-objects (parse-s3-url url)))
  ([bucket prefix]
   (list-objects bucket prefix {}))
  ([bucket prefix opts]
   (invoke :list-objects-v2 (merge opts {:bucket bucket :prefix prefix}))))

(defn list-objects-seq
  ([url]
   (apply list-objects-seq (parse-s3-url url)))
  ([bucket prefix]
   (u/scrolling-seq
    (fn [token]
      (let [result (list-objects bucket prefix
                                 (when token
                                   {:continuation-token token}))
            {:keys [contents
                    is-truncated
                    next-continuation-token]} result]
        [contents
         (when is-truncated next-continuation-token)])))))

(defn head-object
  ([url]
   (apply head-object (parse-s3-url url)))
  ([bucket key]
   (invoke :head-object {:bucket bucket :key (name key)})))
