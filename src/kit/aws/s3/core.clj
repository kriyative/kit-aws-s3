(ns kit.aws.s3.core)

(defmulti init-client! :client-type)
(defmulti invoke-client (fn [client request] (:client-type client)))
