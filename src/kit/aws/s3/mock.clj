(ns kit.aws.s3.mock
  (:require
   [cognitect.anomalies :as ca]
   [kit.aws.s3.core :refer [init-client! invoke-client]])
  (:import
   [java.io BufferedInputStream ByteArrayInputStream]))

(defn bucket-not-found? [client bucket]
  (when-not (@(:buckets client) bucket)
    {::ca/category ::ca/not-found}))

(defn bucket-not-empty? [client bucket]
  (when (not-empty (get-in @(:buckets client) [bucket :objects]))
    {::ca/category ::ca/incorrect}))

(defn object-not-found? [client bucket k]
  (when-not (get-in @(:buckets client) [bucket :objects k])
    {::ca/category ::ca/not-found}))

(defmulti invoke (fn [client op request] op))

(defmethod invoke :default [op request]
  (throw (ex-info "Unimplemented operation"
                  {:op op
                   :request request
                   :anomanly ::unimplemented})))

(defmethod invoke :GetBucketLocation [client _ {:keys [Bucket]}]
  (let [{:keys [region]} (@(:buckets client) Bucket)]
    (if region
      {:LocationConstraint region}
      {::ca/category ::ca/not-found})))

(defmethod invoke :CreateBucket [client _ {:keys [Bucket]}]
  (let [bucket {:Name Bucket
                :region (get-in client [:spec :aws-client-spec :region])
                :objects {}}]
    (swap! (:buckets client) assoc Bucket bucket)
    bucket))

(defmethod invoke :ListBuckets [client _ {:keys [Bucket]}]
  {:Buckets (vals @(:buckets client))})

(defmethod invoke :DeleteBucket [client _ {:keys [Bucket]}]
  (or (bucket-not-found? client Bucket)
      (bucket-not-empty? client Bucket)
      (swap! (:buckets client) dissoc Bucket)))

(defmethod invoke :PutObject [client _ {:keys [Bucket Key Body]}]
  (or (bucket-not-found? client Bucket)
      (swap! (:buckets client) assoc-in [Bucket :objects Key] {:Key Key :Body Body})))

(defmethod invoke :GetObject [client _ {:keys [Bucket Key]}]
  (or (bucket-not-found? client Bucket)
      (object-not-found? client Bucket Key)
      (let [data (get-in @(:buckets client) [Bucket :objects Key :Body])]
        {:Body (BufferedInputStream. (ByteArrayInputStream. data))})))

(defmethod invoke :DeleteObject [client _ {:keys [Bucket Key]}]
  (or (bucket-not-found? client Bucket)
      (object-not-found? client Bucket Key)
      (swap! (:buckets client) (fn [bs]
                          (let [new-objects (-> bs
                                                (get-in [Bucket :objects])
                                                (dissoc Key))]
                            (assoc-in bs [Bucket :objects] new-objects))))))

(defmethod invoke :ListObjectsV2 [client _ {:keys [Bucket]}]
  (or (bucket-not-found? client Bucket)
      {:Contents (map #(select-keys % [:Key]) (vals (:objects (@(:buckets client) Bucket))))}))


(defmethod init-client! :mock [spec]
  {:client-type :mock
   :spec spec
   :buckets (atom {})})

(defmethod invoke-client :mock [client {:keys [op request]}]
  (invoke client op request))

