(ns kit.aws.s3.aws
  (:require
   [cognitect.aws.client.api :as aws]
   [kit.aws.core :as ac]
   [kit.aws.s3.core :refer [init-client! invoke-client]]))

(defmethod init-client! :aws [{:keys [aws-client-spec]}]
  (let [client {:client-type :aws
                :client (ac/make-client (merge {:api :s3} aws-client-spec))}]
    (aws/validate-requests (:client client) true)
    client))

(defmethod invoke-client :aws [client request]
  (aws/invoke (:client client) request))

